﻿namespace WebApplication2.Builder
{
    using AutoMapper;
    using WebApplication2.Domain.Models;
    using WebApplication2.Dtos;
    using WebApplication2.Interfaces;

    public class CableBuilder : ICableBuilder
    {
        private Cable cable = new Cable();

        private readonly ICableRepository repository;
        private readonly IMapper mapper;

        private CableBuilder(ICableRepository repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        public static CableBuilder Init(ICableRepository repository, IMapper mapper)
        {
            return new CableBuilder(repository, mapper);
        }

        public Cable Build() => cable;


        public ICableBuilder FromDto(CableDto dto)
        {
            cable = mapper.Map<Cable>(dto);

            var maybeAnotherCable = repository.GetById(cable.Property1);
            // better call an injected service here 
            cable.ComplexProperty1 = "Hier wurden complexe Dinge errechnet durch eine repository berechnet.";

            return this;
        }

        public ICableBuilder ComplexStuffHere(int aParameter)
        {
            // better call an injected service here

            cable.ComplexProperty2 = $"Gier wurden complexe Dinge mit einem Parameter {aParameter} errechnet.";

            return this;
        }
    }
}

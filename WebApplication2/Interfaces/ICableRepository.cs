﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication2.Domain.Models;

namespace WebApplication2.Interfaces
{
    public interface ICableRepository
    {
        Cable GetById(int id);
    }
}

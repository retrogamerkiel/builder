﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication2.Domain.Models;
using WebApplication2.Dtos;

namespace WebApplication2.Interfaces
{
    public interface ICableBuilder
    {
        ICableBuilder FromDto(CableDto dto);

        ICableBuilder ComplexStuffHere(int aParameter);

        Cable Build();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication2.Domain.Models
{
    public class Cable
    {
        public int Property1 { get; set; }

        public string Property2 { get; set; }

        public string Property3 { get; set; }

        public string ComplexProperty1 { get; set; }

        public string ComplexProperty2 { get; set; }
    }
}

﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using WebApplication2.Builder;
using WebApplication2.Domain.Models;
using WebApplication2.Dtos;
using WebApplication2.Interfaces;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IMapper mapper;
        private readonly ICableBuilder cableBuilder;

        public HomeController(ILogger<HomeController> logger, IMapper mapper, ICableBuilder cableBuilder)
        {
            _logger = logger;
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.cableBuilder = cableBuilder;
        }

        public IActionResult Index()
        {
            var cableDto = CreateCableDto();

            var cable = mapper.Map<Cable>(cableDto);

            cable.ComplexProperty1 = GetComplexProperty1();
            cable.ComplexProperty2 = GetComplexProperty2();

            return View(cable);
        }

        public IActionResult IndexBuilder()
        {
            var cableDto = CreateCableDto();

            var cable = cableBuilder
                .FromDto(cableDto)
                .ComplexStuffHere(12)
                .Build();

            return View(cable);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        private static string GetComplexProperty1()
        {
            return "Hier  in einer Helper Klasse bzw. Service passieren jetzt viele aufregende Dinge.";
        }

        private static string GetComplexProperty2()
        {
            return "Hier oder in einer Helper Klasse bzw. Service passieren jetzt nochmal viele aufregende Dinge.";
        }

        // kommt eigentlich vom webservice handler
        private static CableDto CreateCableDto()
        {
            return new CableDto
            {
                Property1 = 1,
                Property2 = "Property2",
                Property3 = "Property3",
            };
        }
    }
}

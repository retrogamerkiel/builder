﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication2.Dtos
{
    public class CableDto
    {
        public int Property1 { get; set; }

        public string Property2 { get; set; }

        public string Property3 { get; set; }
    }
}

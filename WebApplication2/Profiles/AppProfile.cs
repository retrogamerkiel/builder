﻿namespace WebApplication2.Profiles
{
    using AutoMapper;
    using WebApplication2.Domain.Models;
    using WebApplication2.Dtos;

    public class AppProfile : Profile
    {
        public AppProfile()
        {
            CreateMap<CableDto, Cable>()
                .ForMember(x => x.ComplexProperty1, opt => opt.Ignore())
                .ForMember(x => x.ComplexProperty2, opt => opt.Ignore());
        }
    }
}

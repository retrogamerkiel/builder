﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication2.Domain.Models;
using WebApplication2.Interfaces;

namespace WebApplication2.Repositories
{
    public class CableRepository : ICableRepository
    {
        public Cable GetById(int id)
        {
            return new Cable
            {
                Property1 = 10,
                Property2 = "Ein anderes Kabel Property 2",
                Property3 = "Ein anderes Kabel Property 3",
                ComplexProperty1 = "Ein anderes Kabel ComplexProperty1",
                ComplexProperty2 = "Ein anderes Kabel ComplexProperty2"
            };
        }
    }
}
